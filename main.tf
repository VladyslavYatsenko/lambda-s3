provider "aws" {
  access_key = "AKIA4ESNZVXFCOELDFDI"
  secret_key = "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"
  region = "eu-west-3"
}

resource "aws_s3_bucket" "s3_bucket" {
  bucket = "7eve7s-s3"
  acl = "private"
  tags = {
    Name = "S3 bucket for lambda"
    Environment = "Dev"
  }
  force_destroy = true
}

resource "aws_lambda_function" "java-8-lambda-function" {
  function_name = "java-8-lambda-function"
  filename = "./target/aws-lambda.jar"
  timeout = 10
  memory_size = 512
  handler = "com.company.App::handleRequest"
  runtime = "java8"
  role = aws_iam_role.iam_role_for_lambda.arn
  tags = {
    Name = "Lambda S3"
  }
  environment {
    variables = {
      s3BucketName = "7eve7s-s3"
      ec2Host="http://ec2-3-121-186-88.eu-central-1.compute.amazonaws.com"
    }
  }
}

#iam role
resource "aws_iam_role" "iam_role_for_lambda" {
  name = "lambdaRole"
  description = "Role full access to S3 and work with cloudwatch"
  assume_role_policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
      {
        "Sid": "",
        "Effect": "Allow",
        "Principal": {
          "Service": "lambda.amazonaws.com"
        },
       "Action": "sts:AssumeRole"
      }
    ]
}
EOF
}

#lambda policy
resource "aws_iam_policy" "policy_for_cloud_watch" {
  name = "cloudWatch"
  path = "/"
  description = "IAM policy for logging into Cloud Watch"

  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": [
        "logs:CreateLogStream",
        "logs:PutLogEvents"
      ],
      "Resource": "arn:aws:logs:*:*:*",
      "Effect": "Allow"
    }
  ]
}
EOF

}

# lambda policy
resource "aws_iam_policy" "policy_for_s3" {
  name = "s3Bucket"
  path = "/"
  description = "IAM policy for get/put object into s3 bucket"

  policy = <<EOF
{
    "Version": "2012-10-17",
    "Statement": [
        {
            "Effect": "Allow",
            "Action": ["s3:GetObject", "s3:PutObject"],
            "Resource": "*"
        }
    ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "cloud_watch" {
  role = aws_iam_role.iam_role_for_lambda.name
  policy_arn = aws_iam_policy.policy_for_cloud_watch.arn
}

resource "aws_iam_role_policy_attachment" "s3_bucket_full_access" {
  role = aws_iam_role.iam_role_for_lambda.name
  policy_arn = aws_iam_policy.policy_for_s3.arn
}
