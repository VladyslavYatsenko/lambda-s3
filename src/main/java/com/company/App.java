package com.company;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3Client;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import java.io.IOException;

public class App implements RequestHandler<Object, String> {


    private AmazonS3 amazonS3;
    private static final String FILE_S3 = "test.txt";

    public String getBody() throws IOException {
        HttpClient client = HttpClientBuilder.create().build();
        HttpGet request = new HttpGet(System.getenv("ec2Host") + "/api/v1/s3data");
        client.execute(request);
        HttpResponse response=client.execute(request);
        HttpEntity entity = response.getEntity();
        return EntityUtils.toString(entity, "UTF-8");

    }

    public void saveFileIntoS3(String bucketName, String fileName, String body) {
        amazonS3 = AmazonS3Client.builder().build();
        amazonS3.putObject(bucketName, fileName, body);
    }

    @Override
    public String handleRequest(Object o, Context context) {
        saveFileIntoS3(System.getenv("s3BucketName"), FILE_S3, "I`ll be back");
        context.getLogger().log("Input " + context);
        try {
            context.getLogger().log("Response Body->" + getBody());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return "Done";
    }


}
